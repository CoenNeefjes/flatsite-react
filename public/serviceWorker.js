let CACHE_NAME = 'dirty-a-cache';
const urlsToCache = [
    '/',
    '/index.html',
    '/manifest.json',
    '/ic_launcher_72.png',
    '/ic_launcher_96.png',
    '/ic_launcher_144.png',
    '/ic_launcher_192.png',
    '/splash_4695.png',
    '/static/js/main.chunk.js',
    '/static/js/bundle.js',
    '/static/js/0.chunk.js'
];

self.addEventListener('install', function(event) {
    // Perform install steps
    event.waitUntil(
        caches.open(CACHE_NAME)
            .then(function(cache) {
                console.log('Opened cache');
                return cache.addAll(urlsToCache);
            })
    );
});

self.addEventListener('fetch', function(event) {
    event.respondWith(async function() {
        try{
            let res = await fetch(event.request);
            let cache = await caches.open('cache');
            cache.put(event.request.url, res.clone());
            return res;
        } catch(error) {
            return caches.match(event.request);
        }
    }());
});
