import React, {useEffect, useState} from "react";
import Modal from 'react-modal';
import styled from "styled-components";
import {Row} from "../../../styles/Layout";

Modal.setAppElement('#root');

const Page = styled.div`
    width: 300px;
`;

const Title = styled.div`
    padding-top: 10px;
    padding-bottom: 10px;
    border-bottom: 1px solid black;
`;

const Content = styled.div`
    margin-top: 10px;
`;

const CustomRow = styled(Row)`
    margin-bottom: 5px;
    justify-content: space-between;
`;

const ButtonWrapper = styled.div`
    margin-top: 10px;
    display: flex;
    flex-direction: row;
    justify-content: flex-end;
`;

const customStyles = {
    content : {
        top                   : '50%',
        left                  : '50%',
        right                 : 'auto',
        bottom                : 'auto',
        marginRight           : '-50%',
        transform             : 'translate(-50%, -50%)'
    }
};

const initialFormValues = {
    username: null,
    password: null,
    roles: null,
    active: null
};

const validateFormValues = (formValues) => {
    if (!formValues.username) {
        alert('Please enter a valid username');
        return false;
    }
    if (formValues.roles.split(", ").length === 0) {
        alert('Please enter a correct list of roles separated by: \', \'');
        return false;
    }
    if (formValues.active !== 'true' && formValues.active !== 'false') {
        alert('Please use true or false as value for the active field');
        return false;
    }
    return true;
};

const EditUserModal = ({isOpen = false, closeAction = () => {}, user = null, editAction = () => {}}) => {

    const [formValues, setFormValues] = useState(initialFormValues);

    useEffect(() => {
        if (user?.id) {
            setFormValues({
                username: user.username,
                roles: user.roles.join(', '),
                active: user.active.toString()
            });
        }
    }, [user?.id]);

    const handleSubmit = () => {
        if (validateFormValues(formValues)) {
            editAction(
                user?.id,
                {
                    ...formValues,
                    roles: formValues.roles.split(", ")
                },
                closeAction
            );
        }
    };

    return (
        <Modal
            isOpen={isOpen}
            onRequestClose={closeAction}
            style={customStyles}
            contentLabel="Edit User Modal"
            overlayClassName="Overlay"
        >
            <Page>
                <Title>
                    Edit user: {user?.username}
                </Title>
                <Content>
                    <CustomRow>
                        Username
                        <input
                            type={'text'}
                            value={formValues.username}
                            onChange={(event => setFormValues({...formValues, username: event.target.value}))}
                        />
                    </CustomRow>
                    <CustomRow>
                        Roles
                        <input
                            type={'text'}
                            value={formValues.roles}
                            onChange={(event => setFormValues({...formValues, roles: event.target.value}))}
                        />
                    </CustomRow>
                    <CustomRow>
                        Active
                        <input
                            type={'text'}
                            value={formValues.active}
                            onChange={(event => setFormValues({...formValues, active: event.target.value}))}
                        />
                    </CustomRow>
                    <CustomRow>
                        New Password
                        <input
                            type={'text'}
                            value={formValues.password}
                            onChange={(event => setFormValues({...formValues, password: event.target.value}))}
                        />
                    </CustomRow>
                    <ButtonWrapper>
                        <button
                            type={'button'}
                            onClick={closeAction}
                        >
                            Cancel
                        </button>
                        {' '}
                        <button
                            type={'button'}
                            onClick={handleSubmit}
                        >
                            Save
                        </button>
                    </ButtonWrapper>
                </Content>
            </Page>
        </Modal>
    );
};

export default EditUserModal;