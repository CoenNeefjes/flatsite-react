import React, {useContext, useEffect, useState} from "react";
import styled from "styled-components";
import {colors} from "../../../../util/Colors";
import CustomModal from "../../CustomModal";
import {Row} from "../../../styles/Layout";
import {func} from "../../../../util/Helpers";
import {AlbumContext} from "../../../../shared/context/AlbumsContext";

const TitleHolder = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    
    padding-bottom: 20px;
    padding-top: 20px;
    
    border-style: solid;
    border-width: 0px 0px 1px 0px;
    border-color: ${colors.borderLightGrey};
    
    height: 26px;
`;

const Title = styled.div`
    font-size: 18px;
`;

const CloseHolder = styled.div`
    position: absolute;
    right: 0;
    cursor: pointer;
    margin-right: 20px;
    font-size: 22px;
`;

const Body = styled.div`
    display: flex;
    padding: 20px;
    flex-direction: column;
`;

const Footer = styled.div`
    position: absolute;
    bottom: 0;
    left: 0;
    right: 0;
    
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: flex-end;
    flex: 1;
    
    padding: 20px;
    
    border-style: solid;
    border-width: 1px 0px 0px 0px;
    border-color: ${colors.borderLightGrey};
`;

const CustomRow = styled(Row)`
    margin-bottom: 5px;
`;

const Half = styled.div`
    width: 50%;
`;

const initialFormValues = {
    name: '',
};

const CreateAlbumModal = ({ isOpen = false, setIsOpen = func }) => {

    const [ formValues, setFormValues ] = useState(initialFormValues);

    const { createAlbum } = useContext(AlbumContext);

    useEffect(() => {
        if (!isOpen) {
            setFormValues(initialFormValues);
        }
    }, [isOpen]);

    const handleNameChange = (e) => {
        setFormValues({
            ...formValues,
            name: e.target.value
        });
    };

    const handleSubmit = () => {
        if (!formValues.name) {
            alert('Vul een naam in');
            return;
        }

        createAlbum(formValues, onSuccess)
    };

    const onSuccess = () => {
        setIsOpen(false);
    };

    return (
        <CustomModal isOpen={isOpen} setIsOpen={setIsOpen}>
            <TitleHolder>
                <Title><b>Nieuw Album</b></Title>
                <CloseHolder onClick={() => setIsOpen(false)}>X</CloseHolder>
            </TitleHolder>
            <Body>
                <CustomRow>
                    <Half>Naam:</Half>
                    <Half>
                        <input
                            type={'text'}
                            value={formValues.name}
                            onChange={handleNameChange}
                            style={{ width: '75%' }}
                        />
                    </Half>
                </CustomRow>
            </Body>
            <Footer>
                <button type={'button'} onClick={handleSubmit}>Save</button>
            </Footer>
        </CustomModal>
    );
};

export default CreateAlbumModal;