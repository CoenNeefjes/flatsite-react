import React, {useContext,} from "react";
import {MeContext} from "../../../shared/context/MeContext";
import {BeerFridgeContext} from "../../../shared/context/BeerFridgeContext";
import {isFeut} from "../../../util/Helpers";
import {BeerNumber, GreenText, RedText} from "../../styles/FridgeTexts";

const BeerCountMessage = ({ beerSynonym, message }) => {

    const {me} = useContext(MeContext);
    const {fridgeWeightBeers} = useContext(BeerFridgeContext);

    const synonyms = ["groene palen","pretcilinders","bakken","goude rakkers","pilsjes","pijpjes","biertjes","lagers","koude kletsers"]
    const enough = ["Gelukkig kunnen we hier nog wel even mee", "Komt de feut goed mee weg","Genoeg voor een goede avond!","Dat is even fijn!","Dat wordt lekker smikkelen","Nou, geef me dan maar een pilsie!","Genoeg om een adtje te trekken!"]
    const bijvullenkut = ["Het bier moet bijgevuld worden, kutfeut","Waar blijft de pils nou kutfeut?","Wees niet zo'n klier, haal nog wat bier!","Ga nou toch is bier halen kutfeut"]
    const bijvullen = ["Het bier moet bijgevuld worden","Waar blijft de pils nou?","Te weinig bier voor werkelijk plezier","Te weinig bier voor ons vertier", "Niet genoeg pils, voor ieder wat wils"]

    const getSynonym = () => {
        return synonyms[Math.floor(Math.random() * synonyms.length)]
    }

    const generateGoodSentence = () => {
        if (fridgeWeightBeers === 69) {
            return "nice"
        }
        return enough[Math.floor(Math.random() * enough.length)]
    }

    const generateBadSentence = (me) => {
        if (fridgeWeightBeers === 69) {
            return "nice"
        }
        if (isFeut(me)) {
            return bijvullenkut[Math.floor(Math.random() * bijvullenkut.length)]
        } else {
            return bijvullen[Math.floor(Math.random() * bijvullen.length)]
        }
    }

    return (
        <React.Fragment>
                <span>Er liggen </span>
                <BeerNumber>{fridgeWeightBeers || 0}</BeerNumber>
                <span> {getSynonym()} in de koelkast</span>
                <hr />

                { fridgeWeightBeers >= 24 &&
                <GreenText>{generateGoodSentence()}</GreenText>
                }

                { fridgeWeightBeers < 24 &&
                <RedText>{generateBadSentence(me)}</RedText>
                }
        </React.Fragment>
    );

}

export default BeerCountMessage;