import styled from "styled-components";
import React, {useContext, useEffect} from "react";
import {MeContext} from "../../../shared/context/MeContext";
import PageWrapper from "../PageWrapper";
import {BeerFridgeContext} from "../../../shared/context/BeerFridgeContext";
import {hasRole, isAdmin} from "../../../util/Helpers";
import BeerCountMessage from "./BeerCountMessage";
import AdminButtonsWrapper from "./admin/AdminButtonsWrapper";
import {constants} from "../../../util/Constants";

const Page = styled.div`
    padding: 10px;
    font-family: Verdana;
`;

const BeerFridgePage = () => {

    const {me, loadMe} = useContext(MeContext);
    const {getFridgeWeightKg, getFridgeWeightBeers, getCalibrationPoints, getBeerWeight} = useContext(BeerFridgeContext);

    const canViewControls = isAdmin(me) || hasRole(me, constants.roles.beerScaleAdmin);

    useEffect(() => {
        loadMe();
        getFridgeWeightBeers();

    }, []);

    useEffect(() => {
        if (canViewControls) {
            getFridgeWeightKg();
            getCalibrationPoints();
            getBeerWeight();
        }
    }, [me]);

    return (
        <PageWrapper>
            <Page>
                <BeerCountMessage/>
                {canViewControls && (
                    <AdminButtonsWrapper/>
                )}
            </Page>
        </PageWrapper>
    );

}

export default BeerFridgePage;