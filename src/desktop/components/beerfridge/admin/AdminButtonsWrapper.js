import React, {useState} from "react";
import styled from "styled-components";
import CalibrationPoints from "./CalibrationPoints";
import WeightPerBeerSettings from "./WeightPerBeerSettings";
import CalibrationPointSettings from "./CalibrationPointSettings";
import EndpointsSettings from "./EndpointsSettings";
import WiFiSettings from "./WiFiSettings";
import ApiSettings from "./ApiSettings";
import TareSettings from "./TareSettings";
import HostnameSettings from "./HostnameSettings";
import ButtonWrapper from "../../../styles/ButtonWrapper";
import Button from "../../../styles/Button";

/*
TODO:
- Add info on-hover boxes
 */

const Grid = styled.div`
    margin-top: 20px;
    display: grid;
    grid-template-columns: max-content auto;
    column-gap: 10px;
    row-gap: 10px;
`;

const GridItem = styled.div`
    
`;

const AdminButtonsWrapper = () => {

    const [show, setShow] = useState(false);

    const toggleAdminControls = () => {
        setShow(!show);
    }

    return (
        <React.Fragment>
            <ButtonWrapper>
                <Button onClick={toggleAdminControls}>Show admin controls</Button>
            </ButtonWrapper>
            {
                show && (
                    <React.Fragment>
                        <CalibrationPoints />
                        <Grid>
                            <GridItem>
                                <WeightPerBeerSettings />
                            </GridItem>
                            <GridItem>
                                <CalibrationPointSettings />
                            </GridItem>
                            <GridItem>
                                <EndpointsSettings />
                            </GridItem>
                            <GridItem>
                                <WiFiSettings />
                            </GridItem>
                            <GridItem>
                                <ApiSettings />
                            </GridItem>
                            <GridItem>
                                <TareSettings />
                            </GridItem>
                            <GridItem>
                                <HostnameSettings />
                            </GridItem>
                        </Grid>
                    </React.Fragment>
                )
            }
        </React.Fragment>
    );
};

export default AdminButtonsWrapper;
