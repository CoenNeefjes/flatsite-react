import React from "react";
import {changeApiRequest} from "../../../../shared/network/BeerFridgeRequests";
import ButtonWrapper from "../../../styles/ButtonWrapper";
import Button from "../../../styles/Button";
import ButtonInputField from "../../../styles/ButtonField";

const ApiSettings = () => {

    let newApiHost = null;
    let newApiPort = null;

    const handleApiHostInput = (event) => {
        newApiHost = event.target.value;
    }

    const handleApiPortInput = (event) => {
        newApiPort = event.target.value;
    }

    const changeApi = () => {
        const port = parseInt(newApiPort)
        if (newApiHost.length > 40) {
            alert("Max 40 karakters aub");
            return;
        }
        if (isNaN(port))  {
            alert("Vul een getal in");
            return;
        }
        if (port <= 0 || port > 65535) {
            alert("Ongeldige poort");
            return;
        }
        if (!port || !newApiHost) {
            alert("Niet alles is ingevuld");
            return;
        }

        changeApiRequest({"hostname": newApiHost, "port": port}).then();

    }

    return (
        <React.Fragment>
            <b>API</b>
            <br/>
            <ButtonInputField
                onChange={handleApiHostInput}
                placeholder="website.nl/api"
            />
            <ButtonInputField
                onChange={handleApiPortInput}
                placeholder="5000"
            />
            <br/>
            <ButtonWrapper>
                <Button onClick={changeApi}>Verander</Button>
            </ButtonWrapper>
        </React.Fragment>
    );
};

export default ApiSettings;
