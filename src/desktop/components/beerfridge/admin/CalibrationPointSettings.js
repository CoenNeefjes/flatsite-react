import React from "react";
import {addCalibrationRequest, deleteCalibrationPointsRequest} from "../../../../shared/network/BeerFridgeRequests";
import ButtonWrapper from "../../../styles/ButtonWrapper";
import Button from "../../../styles/Button";
import SmallerInputField from "../../../styles/SmallerInputField";

const CalibrationPointSettings = () => {

    let newCalibrationWeight = null;
    let newMeasureCount = null;

    const handleCalibrationWeightInput = event => {
        newCalibrationWeight = event.target.value;
    }

    const handleMeasureCountInput = event => {
        newMeasureCount = event.target.value;
    }

    const addCalibrationWeight = () => {
        const calibrationweight = parseFloat(newCalibrationWeight); // can be 0
        const measuresnumber = parseFloat(newMeasureCount);

        if (isNaN(calibrationweight) || isNaN(measuresnumber)) {
            alert("Vul een getal in");
            return;
        }
        if (calibrationweight < 0) {
            alert("Het kalibratiegewicht moet positief zijn");
            return;
        }
        if (measuresnumber <= 0) {
            alert("Het aantal metingen moet groter dan 0 zijn");
            return;
        }

        addCalibrationRequest({"calibrationweight": calibrationweight, "measuresnumber": measuresnumber}).then();
    }

    return (
        <React.Fragment>
            <b>Kalibratiepunt</b>
            <br/>
            <SmallerInputField
                onChange={handleCalibrationWeightInput}
                placeholder="5"
            /> kg
            <br />
            <SmallerInputField
                onChange={handleMeasureCountInput}
                placeholder="10"
            /> metingen
            <br/>
            <ButtonWrapper>
                <Button onClick={addCalibrationWeight}>Voeg toe</Button>
            </ButtonWrapper>
            <br />
            <ButtonWrapper>
                <Button onClick={deleteCalibrationPointsRequest}>Verwijder alle punten</Button>
            </ButtonWrapper>
        </React.Fragment>
    );
};

export default CalibrationPointSettings;
