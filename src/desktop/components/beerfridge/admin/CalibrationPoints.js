import React, {useContext} from "react";
import styled from "styled-components";
import {BeerFridgeContext} from "../../../../shared/context/BeerFridgeContext";

const TableWrapper = styled.div`
    overflow-y: auto; 
    height: 120px;
`;

const Table = styled.table`
    margin-top: 10px;
    font-size: 12px;
    border-collapse: collapse;
    width: 100%;
`;

const Tr = styled.tr`
    :nth-child(even) {
        background-color: #dddddd;
    } 
`;

const Td = styled.td`
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
`;

const Th = styled.th`
    position: sticky; 
    top: 0;
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
    ${props => props.width && `width: ${props.width}px;`}
`;

const THead = styled.thead`
    position: sticky; 
    top: 0;
    background-color: #dddddd;
`;


const CalibrationPoints = () => {

    const {calibrationPoints} = useContext(BeerFridgeContext);

    return (
        <React.Fragment>
            <b>Kalibratiepunten</b>
            <TableWrapper>
                <Table>
                    <THead>
                        <Tr>
                            <Th>Gewicht</Th>
                            <Th>Gemiddelde meting</Th>
                        </Tr>
                    </THead>
                    <tbody>
                        {
                        calibrationPoints.map((x, i) => (
                            <Tr key={i}>
                                <Td>{x.weight} kg</Td>
                                <Td>{x.averagereading}</Td>
                            </Tr>
                            ))}
                    </tbody>
                </Table>
            </TableWrapper>
        </React.Fragment>
    );
};

export default CalibrationPoints;
