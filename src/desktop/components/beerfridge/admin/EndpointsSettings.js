import React from "react";
import {changeEndpointRequest} from "../../../../shared/network/BeerFridgeRequests";
import ButtonWrapper from "../../../styles/ButtonWrapper";
import Button from "../../../styles/Button";
import ButtonInputField from "../../../styles/ButtonField";

const EndpointsSettings = () => {

    let newMeasureEndpoint = null;
    let newCalibrateEndpoint = null;

    const handleMeasureEndpoint = event => {
        newMeasureEndpoint = event.target.value;
    }

    const handleCalibrateEndpoint = event => {
        newCalibrateEndpoint = event.target.value;
    }

    const changeEndpoints = () => {
        if (newCalibrateEndpoint.length > 30 || newMeasureEndpoint.length > 30) {
            alert("Max 30 karakters");
            return;
        }
        if (!newCalibrateEndpoint || !newMeasureEndpoint) {
            alert("Niet alles is ingevuld");
            return;
        }

        changeEndpointRequest(
            {"measureendpoint": newMeasureEndpoint,
                "calibrationendpoint": newCalibrateEndpoint}
                ).then();
    }

    return (
        <React.Fragment>
            <b>Endpoints</b>
            <br/>
            Meting-endpoint:
            <ButtonInputField
                onChange={handleMeasureEndpoint}
                placeholder="/measure"
            />
            <br/>
            Kalibratie-endpoint:
            <ButtonInputField
                onChange={handleCalibrateEndpoint}
                placeholder="/calibrate"
            />
            <br/>
            <ButtonWrapper>
                <Button onClick={changeEndpoints}>Verander</Button>
            </ButtonWrapper>
        </React.Fragment>
    );
};

export default EndpointsSettings;
