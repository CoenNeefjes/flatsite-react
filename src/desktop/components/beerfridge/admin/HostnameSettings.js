import React from "react";
import {changeScaleHostNameRequest} from "../../../../shared/network/BeerFridgeRequests";
import Button from "../../../styles/Button";
import ButtonWrapper from "../../../styles/ButtonWrapper";
import ButtonInputField from "../../../styles/ButtonField";

const HostnameSettings = () => {

    let newScaleHostName = null;

    const handleScaleHostNameInput = (event) => {
        newScaleHostName = event.target.value;
    }

    const changeScaleHostName = () => {
        if (!newScaleHostName) {
            alert("Niet alles is ingevuld");
            return;
        }
        changeScaleHostNameRequest({"hostname": newScaleHostName}).then();
    }

    return (
        <React.Fragment>
            <b>Weegschaal hostname</b>
            <br/>
            <ButtonInputField
                onChange={handleScaleHostNameInput}
                placeholder="http://hostname.nl:1000"
            />
            <br/>
            <ButtonWrapper>
                <Button
                    onClick={changeScaleHostName}>Verander</Button>
            </ButtonWrapper>
        </React.Fragment>
    );
};

export default HostnameSettings;
