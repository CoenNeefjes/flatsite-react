import React from "react";
import {tareScaleRequest} from "../../../../shared/network/BeerFridgeRequests";
import Button from "../../../styles/Button";
import ButtonWrapper from "../../../styles/ButtonWrapper";

const TareSettings = () => {

    const tare = () => {
        tareScaleRequest().then();
    }

    return (
        <React.Fragment>
            <b>Zet nulpunt</b>
            <br/>
            <ButtonWrapper>
                <Button onClick={tare}>Zet</Button>
            </ButtonWrapper>
        </React.Fragment>
    );
};

export default TareSettings;
