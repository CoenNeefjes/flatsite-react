import React, {useContext} from "react";
import {changeBeerWeightRequest} from "../../../../shared/network/BeerFridgeRequests";
import {BeerFridgeContext} from "../../../../shared/context/BeerFridgeContext";
import ButtonWrapper from "../../../styles/ButtonWrapper";
import Button from "../../../styles/Button";
import SmallerInputField from "../../../styles/SmallerInputField";
import {SmallText} from "../../../styles/FridgeTexts";

const WeightPerBeerSettings = () => {

    const { fridgeWeightKg, beerWeight} = useContext(BeerFridgeContext);

    let newBeerWeight = null;

    const handleBeerWeightInput = event => {
        newBeerWeight = event.target.value;
    }

    const changeBeerWeightValue = () => {
        const value = parseFloat(newBeerWeight);

        if (isNaN(value)) {
            alert("Vul een getal in");
            return;
        }
        if (value <= 0) {
            alert("Gewicht moet groter dan 0 zijn");
            return;
        }

        changeBeerWeightRequest({"weight": value}).then();
    }

    return (
        <React.Fragment>
            <b>Gewicht per biertje</b>
            <br/>
            <SmallerInputField
                onChange={handleBeerWeightInput}
                placeholder={beerWeight}
            /> kg
            <br/>
            <SmallText>Er ligt nu {fridgeWeightKg}kg in de koelkast</SmallText>
            <br />
            <ButtonWrapper>
                <Button onClick={changeBeerWeightValue}>Verander</Button>
            </ButtonWrapper>
        </React.Fragment>
    );
};

export default WeightPerBeerSettings;
