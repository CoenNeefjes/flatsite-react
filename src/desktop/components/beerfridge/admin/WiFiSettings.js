import React from "react";
import {changeWifiRequest} from "../../../../shared/network/BeerFridgeRequests";
import ButtonWrapper from "../../../styles/ButtonWrapper";
import Button from "../../../styles/Button";
import ButtonInputField from "../../../styles/ButtonField";

const WiFiSettings = () => {

    let newWifiSSID = null;
    let newWifiPassword = null;

    const handleWifiSSIDInput = (event) => {
        newWifiSSID = event.target.value;
    }

    const handleWifiPasswordInput = (event) => {
        newWifiPassword = event.target.value;
    }

    const changeWifi = () => {
        if (newWifiPassword.length > 30 || newWifiSSID.length > 30) {
            alert("Max 30 karakters aub");
            return;
        }
        if (!newWifiSSID || !newWifiPassword) {
            alert("Niet alles is ingevuld");
            return;
        }
        changeWifiRequest({"ssid": newWifiSSID, "password": newWifiPassword}).then();
    }

    return (
        <React.Fragment>
            <b>WiFi</b>
            <br/>
            <ButtonInputField
                onChange={handleWifiSSIDInput}
                placeholder="MyNetwork"
            />
            <ButtonInputField
                onChange={handleWifiPasswordInput}
                placeholder="password123"
            />
            <br/>
            <ButtonWrapper>
                <Button onClick={changeWifi}>Verander</Button>
            </ButtonWrapper>
        </React.Fragment>
    );
};

export default WiFiSettings;
