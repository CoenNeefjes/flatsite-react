import React, {useEffect, useState} from "react";
import dayjs from "dayjs";
import BeerInputModal from "./BeerInputModal";
import {getDescription, getTypeText} from "../../../../shared/util/BeerTypeUtil";
import {initialBeerInput} from "../BeerListPage";

const EditBeerInputModal = ({
    isOpen = false,
    setIsOpen = (value) => {},
    users = [],
    modify = () => {},
    beerInput = {},
    beerTypes = [],
    currentBeerTypeId = null,
    setSelectedBeerTypeId = (value) => {},
    setSelectedBeerInput = (beerInput) => {},
    setShowUploadModal = () => {}
}) => {

    const [ formValues, setFormValues ] = useState(beerInput);
    const [ description, setDescription ] = useState('Aantal');

    useEffect(() => {
        if (beerInput) {
            setFormValues({
                ...beerInput,
                enrollments: beerInput.enrollments.map(x => ({
                    user: x.user.id,
                    amount: x.amount
                }))
            });
        }
    }, [beerInput])

    useEffect(() => {
        if (isOpen) {
            setFormValues({
                ...formValues,
                beerType: currentBeerTypeId
            });
        }
    }, [isOpen])

    useEffect(() => {
        setDescription(getDescription(formValues.type));
    }, [formValues.type])

    const resetBeerInput = () => {
        setFormValues(initialBeerInput);
    }

    const handleTypeChange = (event) => {
        setFormValues({
            ...formValues,
            type: event.target.value
        });
    };

    const handleBeerTypeChange = (event) => {
        setFormValues({
            ...formValues,
            beerType: event.target.value
        });
    };

    const handleEnrollmentChange = (event, userId) => {
        const currentEnrollment = formValues.enrollments.find(x => x.user === userId);
        if (currentEnrollment) {
            // Update enrollment
            setFormValues({
                ...formValues,
                enrollments: formValues.enrollments.map(x => x.user !== userId ? x : {
                    ...x,
                    amount: parseInt(event.target.value)
                })
            });
        } else {
            // Create enrollment
            setFormValues({
                ...formValues,
                enrollments: [...formValues.enrollments, { user: userId, amount: parseInt(event.target.value) }]
            })
        }
    };

    const handleNoteChange = (event) => {
        setFormValues({
            ...formValues,
            note: event.target.value
        });
    };

    const handleSubmit = () => {
        const beerType = beerTypes.find(x => x.id.toString() === formValues.beerType.toString());
        modify({
            ...formValues,
            date: dayjs().format('YYYY-MM-DD'),
            // Filter all zero or negative values
            // And multiply the amount times the beersPerCrate when it is a receipt
            enrollments: formValues.enrollments.filter(x => x.amount > 0).map(x => ({
                ...x,
                amount: formValues.type === 'RECEIPT' ? x.amount * beerType.beersPerCrate : x.amount
            }))
        }, (beerInput) => {
            setSelectedBeerTypeId(formValues.beerType);
            setIsOpen(false);
            setShowUploadModal(true);
            setSelectedBeerInput(beerInput);
            resetBeerInput()
        });
    };

    const inputTypes = (
        <select value={formValues.type || ''} onChange={handleTypeChange}>
            <option value={null} />
            <option value={'LIST'}>Bierlijst</option>
            <option value={'RECEIPT'}>Bonnetje</option>
        </select>
    );

    const beerTypeSelect = (
        <select value={formValues.beerType || ''} onChange={handleBeerTypeChange}>
            <option value={null} />
            {beerTypes.map(x => (
                <option key={x.id} value={x.id}>{x.name}</option>
            ))}
        </select>
    );

    const getUserInput = (user, vals) => {
        return (
            <input
                type={'number'}
                value={vals.enrollments.find(e => e.user === user.id)?.amount || 0}
                onChange={(event) => handleEnrollmentChange(event, user.id)}
                min={0}
            />
        );
    }

    const buildNote = (
        <textarea
            rows={3}
            placeholder={"Optioneel"}
            value={formValues.note || ''}
            onChange={handleNoteChange}
        />
    );

    const title = beerInput.date === null
        ? 'Nieuwe invoer'
        : `Edit "${dayjs(beerInput.date).format('DD/MM') + ' ' + getTypeText(beerInput.type)}"`

    return (
        <BeerInputModal
            title={title}
            isOpen={isOpen}
            setIsOpen={setIsOpen}
            isEdit={true}
            inputTypes={inputTypes}
            beerTypes={beerTypeSelect}
            description={description}
            users={users}
            userEntry={(user) => getUserInput(user, formValues)}
            note={buildNote}
            handleSubmit={handleSubmit}
        />
    );
};

export default EditBeerInputModal;
