import {Half, PanelBody, PanelBottom, PanelContainer, PanelTop} from "../../styles/Panel";
import {Title} from "../../styles/Layout";
import {sortByBalanceDescAndUserIdAsc} from "../../../shared/util/SortUtils";
import React from "react";

const PrintingOverviewPanel = ({ overview = [] }) => {
    return (
        <PanelContainer>
            <PanelTop>
                <Title>Overzicht</Title>
            </PanelTop>
            <PanelBody>
                {overview?.sort(sortByBalanceDescAndUserIdAsc)?.map((x) => (
                    <React.Fragment key={x.user.id}>
                        <Half>{x.user.username}</Half>
                        <Half>{x.balance}</Half>
                    </React.Fragment>
                ))}
            </PanelBody>
            <PanelBottom />
        </PanelContainer>
    );
}

export default PrintingOverviewPanel;