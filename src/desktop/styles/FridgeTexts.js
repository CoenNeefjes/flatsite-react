import styled from "styled-components";


export const RedText = styled.span`
    color: rgb(237, 41, 56);
`;

export const GreenText = styled.span`
    color: rgb(2, 78, 27);
`;

export const BeerNumber = styled.span`
    color: rgb(248, 148, 29);
    font-size: 6em;
    font-family: Arial;
`;

export const SmallText = styled.small`
    color: dimgrey;
`;
