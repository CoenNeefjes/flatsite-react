import styled from 'styled-components';

const SmallerInputField = styled.input`
    width: 60px;
    margin-top: 1px;
    margin-bottom: 1px;
`;

export default SmallerInputField;
