import React from "react";
import styled from "styled-components";
import MobilePageWrapper from "../MobilePageWrapper";
import ActionBar from "../ActionBar";
import {routes} from "../../../shared/routing/Routes";
import useBeerFridge from "../../../shared/hooks/BeerFridgeHook";
import {isFeut} from "../../../util/Helpers";
import {Row} from "../../styles/Grid";
import {colors} from "../../../util/Colors";
import {spacing} from "../../util/Spacing";

const LargeNumber = styled(Row)`
    flex: 1;
    height: 50vh;
    
    justify-content: center;
    align-items: center;
    
    color: ${colors.orange};
    font-size: 100px;
`;

const TextRow = styled(Row)`
    justify-content: center;
    margin-bottom: ${spacing.big}px;
`;

const MobileBeerFridgePage = () => {

    const { beerSynonym, enoughText, addBeerFeutText, addBeerText, me, fridgeWeightBeers } = useBeerFridge();

    const enough = fridgeWeightBeers >= 24;

    const getMessage = () => {
        if (fridgeWeightBeers === 69) return 'Nice';

        if (enough) {
            return enoughText;
        } else {
            if (isFeut(me)) {
                return addBeerFeutText;
            } else {
                return addBeerText;
            }
        }
    }

    return (
        <MobilePageWrapper>
            <ActionBar activeRoute={routes.beerFridge}>
                <LargeNumber>{fridgeWeightBeers || 0}</LargeNumber>
                <TextRow>Er liggen {fridgeWeightBeers || 0} {beerSynonym} in de koelkast</TextRow>
                <TextRow style={{ color: enough ? 'rgb(2, 78, 27)' : 'rgb(237, 41, 56)' }}>{getMessage()}</TextRow>
            </ActionBar>
        </MobilePageWrapper>
    );
}

export default MobileBeerFridgePage;