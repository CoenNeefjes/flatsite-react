import React from "react";
import {TextField} from "@material-ui/core";

const MaterialInput = ({ title = '', value, onChange }) => {
    return (
        <TextField label={title} value={value} onChange={onChange} style={{ width: '100%' }} />
    );
};

export default MaterialInput;