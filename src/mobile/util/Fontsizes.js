export const fontSizes = {
    small: 12,
    base: 14,
    semiBig: 18,
    big: 24,
    input: 16,
};