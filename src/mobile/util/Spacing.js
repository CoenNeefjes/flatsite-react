export const spacing = {
    small: 5,
    base: 10,
    big: 15,
};