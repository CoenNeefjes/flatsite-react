import React, { createContext, useState } from 'react';
import { getFridgeWeightKgRequest, getFridgeWeightBeersRequest, getCalibrationPointsRequest, getBeerWeightRequest} from '../network/BeerFridgeRequests';

export const BeerFridgeContext = createContext({});

const BeerFridgeContextProvider = (props) => {

    const [fridgeWeightKg, setFridgeWeightKg] = useState(null);
    const [fridgeWeightBeers, setFridgeWeightBeers] = useState(null);
    const [calibrationPoints, setCalibrationPoints] = useState([]);
    const [beerWeight, setBeerWeight] = useState(null);

    const getFridgeWeightKg = async () => {
        const result = await getFridgeWeightKgRequest();
        if (result)  {
            setFridgeWeightKg(result);
        }
    };

    const getFridgeWeightBeers = async () => {
        const result = await getFridgeWeightBeersRequest();
        if (result) {
            setFridgeWeightBeers(result);
        }
    };

    const getCalibrationPoints = async () => {
        const result = await getCalibrationPointsRequest();
        if (result) {
            setCalibrationPoints(result);
        }
    };

    const getBeerWeight = async () => {
        const result = await getBeerWeightRequest();
        if (result) {
            setBeerWeight(result);
        }
    };

    return (
        <BeerFridgeContext.Provider value={{
            fridgeWeightKg,
            fridgeWeightBeers,
            calibrationPoints,
            beerWeight,
            getFridgeWeightKg,
            getFridgeWeightBeers,
            getCalibrationPoints,
            getBeerWeight }}>
            {props.children}
        </BeerFridgeContext.Provider>
    );
};

export default BeerFridgeContextProvider;
