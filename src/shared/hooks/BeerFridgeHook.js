import {useContext, useEffect, useState} from "react";
import {MeContext} from "../context/MeContext";
import {BeerFridgeContext} from "../context/BeerFridgeContext";

const beerSynonyms = ["groene palen","pretcilinders","bakken","goude rakkers","pilsjes","pijpjes","biertjes","lagers","koude kletsers"]
const enoughTexts = ["Gelukkig kunnen we hier nog wel even mee", "Komt de feut goed mee weg","Genoeg voor een goede avond!","Dat is even fijn!","Dat wordt lekker smikkelen","Nou, geef me dan maar een pilsie!","Genoeg om een adtje te trekken!"]
const addBeerFeutTexts = ["Het bier moet bijgevuld worden, kutfeut","Waar blijft de pils nou kutfeut?","Wees niet zo'n klier, haal nog wat bier!","Ga nou toch is bier halen kutfeut"]
const addBeerTexts = ["Het bier moet bijgevuld worden","Waar blijft de pils nou?","Te weinig bier voor werkelijk plezier","Te weinig bier voor ons vertier", "Niet genoeg pils, voor ieder wat wils"]

function useBeerFridge() {

    const [ beerSynonym ] = useState(beerSynonyms[Math.floor(Math.random() * beerSynonyms.length)]);
    const [ enoughText ] = useState(enoughTexts[Math.floor(Math.random() * enoughTexts.length)]);
    const [ addBeerFeutText ] = useState(addBeerFeutTexts[Math.floor(Math.random() * addBeerFeutTexts.length)]);
    const [ addBeerText ] = useState(addBeerTexts[Math.floor(Math.random() * addBeerTexts.length)]);

    const { me, loadMe } = useContext(MeContext);
    const { fridgeWeightBeers, getFridgeWeightBeers } = useContext(BeerFridgeContext);

    useEffect(() => {
        loadMe();
        getFridgeWeightBeers();
    }, []);

    return { beerSynonym, enoughText, addBeerFeutText, addBeerText, me, fridgeWeightBeers };

}

export default useBeerFridge;