import Axios from "axios";
import {constants} from "../../util/Constants";
import {defaultAxiosErrorHandler} from "../util/RequestUtils";

// API REQUESTS

export const getFridgeWeightKgRequest = () => Axios({
    method: 'get',
    url: `${constants.apiBaseUrl}/beer-scale/measurement/kg`,
}).then((response) => response.data).catch(defaultAxiosErrorHandler);

export const getFridgeWeightBeersRequest = () => Axios({
    method: 'get',
    url: `${constants.apiBaseUrl}/beer-scale/measurement/beers`,
}).then((response) => response.data).catch(defaultAxiosErrorHandler);

export const getCalibrationPointsRequest = () => Axios({
    method: 'get',
    url: `${constants.apiBaseUrl}/beer-scale/calibration`,
}).then((response) => response.data).catch(defaultAxiosErrorHandler);

export const getBeerWeightRequest = () => Axios({
    method: 'get',
    url: `${constants.apiBaseUrl}/beer-scale/beerweight`,
}).then((response) => response.data).catch(defaultAxiosErrorHandler);

export const changeBeerWeightRequest = (data) => Axios({
    method: 'post',
    url: `${constants.apiBaseUrl}/beer-scale/beerweight`,
    data,
}).then((response) => response.data).catch(defaultAxiosErrorHandler);

export const deleteCalibrationPointsRequest = () => Axios({
    method: 'delete',
    url: `${constants.apiBaseUrl}/beer-scale/calibration`,
}).then((response) => response.data).catch(defaultAxiosErrorHandler);

export const tareScaleRequest = () => Axios({
    method: 'put',
    url: `${constants.apiBaseUrl}/beer-scale/scale/tare`,
}).then((response) => response.data).catch(defaultAxiosErrorHandler);

export const changeEndpointRequest = (data) => Axios({
    method: 'put',
    url: `${constants.apiBaseUrl}/beer-scale/scale/changeendpoint`,
    data,
}).then((response) => response.data).catch(defaultAxiosErrorHandler);

export const changeWifiRequest = (data) => Axios({
    method: 'put',
    url: `${constants.apiBaseUrl}/beer-scale/scale/changewifi`,
    data,
}).then((response) => response.data).catch(defaultAxiosErrorHandler);

export const changeApiRequest = (data) => Axios({
    method: 'put',
    url: `${constants.apiBaseUrl}/beer-scale/scale/changeapi`,
    data,
}).then((response) => response.data).catch(defaultAxiosErrorHandler);

export const addCalibrationRequest = (data) => Axios({
    method: 'post',
    url: `${constants.apiBaseUrl}/beer-scale/scale/addcalibration`,
    data,
}).then((response) => response.data).catch(defaultAxiosErrorHandler);

export const changeScaleHostNameRequest = (data) => Axios({
    method: 'put',
    url: `${constants.apiBaseUrl}/beer-scale/scale/hostname`,
    data,
}).then((response) => response.data).catch(defaultAxiosErrorHandler);