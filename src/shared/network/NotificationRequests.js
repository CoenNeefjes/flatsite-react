import Axios from "axios";
import {constants} from "../../util/Constants";
import {defaultAxiosErrorHandler} from "../util/RequestUtils";

export const getMyNotificationsRequest = () => Axios({
    method: 'get',
    url: `${constants.apiBaseUrl}/notifications/me`
}).then((response) => response.data).catch(defaultAxiosErrorHandler);