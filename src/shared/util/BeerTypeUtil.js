export const getDescription = (type) => {
    switch (type) {
        case 'LIST':
            return 'Aantal biertjes';
        case 'RECEIPT':
            return 'Aantal kratten';
        default:
            return 'Aantal';
    }
}

export const getTypeText = (type) => {
    switch (type) {
        case 'LIST':
            return 'Bierlijst';
        case 'RECEIPT':
            return 'Bonnetje';
        default:
            return 'Error';
    }
}