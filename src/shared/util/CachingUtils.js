export const loadFromCache = async (cacheName, key) => {
    let cache = await caches.open(cacheName);
    return await cache.match(key);
};

export const cacheValue = async (cacheName, key, value) => {
    let cache = await caches.open(cacheName);
    await cache.put(key, value);
};