export const defaultAxiosErrorHandler = (error) => {
    alert(`error: ${error}, message: ${error.response ? error.response.data.message : 'cannot get response from server'}`);
};