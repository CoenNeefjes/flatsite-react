export const getMyTransactionBalanceFromOverview = (transactionBalanceOverview = [], myId = -1) => {
    return transactionBalanceOverview.find(x => x.user.id === myId)?.balance || 0;
};