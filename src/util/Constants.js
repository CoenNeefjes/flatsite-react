export const constants = {
    apiBaseUrl: `${window.location.origin?.replace('3000','8080')}/api`,
    localstorageJwtKey: 'token',
    localStorageSettingsKey: 'settings',
    roles: {
        admin: 'ADMIN',
        user: 'USER',
        feut: 'FEUT',
        beerScaleAdmin: 'BEER_SCALE_ADMIN',
    },
    imageCacheName: 'dirty-a-image-cache',
};
