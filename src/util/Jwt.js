import {constants} from "./Constants";
import Axios from "axios";
import {func} from "./Helpers";

export const handleJwt = (jwt = '', history, onAfterJwtSet = func) => {
    window.localStorage.setItem(constants.localstorageJwtKey, `${jwt}`)
    Axios.defaults.headers.common['Authorization'] = `Bearer ${jwt}`;
    onAfterJwtSet();
    history.push("/");
};